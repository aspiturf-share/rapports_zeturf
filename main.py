import sys
import requests
import datetime
import MysqlUserDb as MySQL
import time
from bs4 import BeautifulSoup
import re


URL = f'https://www.zeturf.fr/fr/programmes-et-pronostics/'

def main(argv):

    courses = search_prog()
    print(courses)



def search_prog():
    """
    Cherche les courses du jour
    """
    data = []
    r = requests.get(URL)
    soup = BeautifulSoup(r.content, 'html5lib')
    table_programme = soup.find('div', {'class': ['programme-wrapper today bloc']})
    table_body = table_programme.find_all('tbody')
    for tbody in table_body:
        rows = tbody.find_all('tr')
    
        for row in rows:    
            # get td with class nom
            td_nom = row.find('td', {'class': 'nom'})
            # print("td_nom", td_nom)
            # get href
            if td_nom is not None:
                link_reunion = td_nom.find('a').get('href')
                data.append(link_reunion)

    print("data ", data)
    # request reunion page
    for link in data:
        print("link: ", link)

        data_reunion = []
        r = requests.get('https://www.zeturf.com' + link)
        soup = BeautifulSoup(r.content, 'html5lib')
        # get table with class programme
        table_reunion = soup.find('table', {'class': 'programme'})
        table_body = table_reunion.find('tbody')
        rows = table_body.find_all('tr', {'class': 'item'})
        for row in rows:
            # print("row", row)
            td_nom = row.find('td', {'class': 'nom'})
            # print("td_nom", td_nom)
            if row is not None:
                link_course = row.find('a').get('href')
                data_reunion.append(link_course)
        print("data_reunion", data_reunion)
        for link_course in data_reunion:
            print("link_course: ", link_course)
            # /fr/course/2022-09-26/R3C1-vichy-prix-de-combronde
            jour = link.split('/')[3]
            reunion_string = link_course.split('/')[4]
            reunion_string = reunion_string.split('-')
            reunion = re.findall(r'R(\d+)', reunion_string[0])[0]
            course = re.findall(r'C(\d+)', reunion_string[0])[0]
            print("jour: ", jour)
            print("reunion: ", reunion)
            print("course: ", course)
            r = requests.get('https://www.zeturf.com' + link_course)
            soup = BeautifulSoup(r.content, 'html5lib')
            table_rapports = soup.find('div', attrs = {'id':'rapports'})
            # print("table_rapports: ", table_rapports)
            if table_rapports is not None:
                table_bloc = table_rapports.find_all('div', {'class': 'table'})
                if table_bloc is None:
                    continue
                for table in table_bloc:
                    # print("*******"*4)
                    rapport_data = {}
                    # print("table: ", table)
                    rapport_list = [rapport_tag.text.strip() for rapport_tag in table.find_all("div", class_="table-cell")]
                    rapport_data['jour'] = jour
                    rapport_data['reunion'] = reunion
                    rapport_data['course'] = course
                    rapport_list = [rapport.replace(' €', '') for rapport in rapport_list]
                    rapport_list = [rapport.replace(',', '.') for rapport in rapport_list]
                    print("rapport_list", rapport_list)

                    if rapport_list[0] == 'Simple':
                        try:
                            index_list = rapport_list.index('ZE couillon')
                        except:
                            index_list = rapport_list.index('Simple placé')
                        # print("index_list", index_list)
                        # rapport_list replace € by empty string
                        

                        print("rapport_list len : ", rapport_list.__len__())
                        # si longueur = 13, seulement 2 arrivés et pas de zecouillon
                        longueur = rapport_list.__len__()

                        rapport_data['rapport_type'] = 'gagnant'
                        rapport_data['horses'] = rapport_list[index_list + 1]
                        rapport_data['rapport'] = rapport_list[index_list + 2]
                        save_mysql(rapport_data)
                        rapport_data['rapport_type'] = 'place1'
                        rapport_data['horses'] = rapport_list[index_list + 1]
                        rapport_data['rapport'] = rapport_list[index_list + 4]
                        save_mysql(rapport_data)
                        # rapport_simple['gagnant'] = {'num': rapport_list[index_list + 1], 'rapport': rapport_list[index_list + 2]}
                        # rapport_simple['place1'] = {'num': rapport_list[index_list + 1], 'rapport': rapport_list[index_list + 4]}
                        try:
                            if longueur == 13:
                                rapport_data['rapport_type'] = 'zeshow'
                                rapport_data['horses'] = rapport_list[index_list + 5]
                                rapport_data['rapport'] = rapport_list[index_list + 7]
                                save_mysql(rapport_data)
                                rapport_data['rapport_type'] = 'place2'
                                rapport_data['horses'] = rapport_list[index_list + 5]
                                rapport_data['rapport'] = rapport_list[index_list + 8]
                                save_mysql(rapport_data)
                            else:
                                rapport_data['rapport_type'] = 'zeshow'
                                rapport_data['horses'] = rapport_list[index_list + 6]
                                rapport_data['rapport'] = rapport_list[index_list + 8]
                                save_mysql(rapport_data)
                                rapport_data['rapport_type'] = 'place2'
                                rapport_data['horses'] = rapport_list[index_list + 6]
                                rapport_data['rapport'] = rapport_list[index_list + 9]
                                save_mysql(rapport_data)
                        except:
                            pass

                        try:
                            rapport_data['rapport_type'] = 'place3'
                            rapport_data['horses'] = rapport_list[index_list + 11]
                            rapport_data['rapport'] = rapport_list[index_list + 14]
                            if rapport_data['horses'] != '':
                                save_mysql(rapport_data)
                        except:
                            pass

                    try:
                        if rapport_list[0] == 'Jumelé':
                            rapport_data['rapport_type'] = 'jumele_gagnant'
                            rapport_data['horses'] = rapport_list[4]
                            rapport_data['rapport'] = rapport_list[5]
                            save_mysql(rapport_data)
                            rapport_data['rapport_type'] = 'jumele_place1'
                            rapport_data['horses'] = rapport_list[4]
                            rapport_data['rapport'] = rapport_list[6]
                            save_mysql(rapport_data)
                            rapport_data['rapport_type'] = 'jumele_place2'
                            rapport_data['horses'] = rapport_list[7]
                            rapport_data['rapport'] = rapport_list[9]
                            save_mysql(rapport_data)
                            rapport_data['rapport_type'] = 'jumele_place3'
                            rapport_data['horses'] = rapport_list[10]
                            rapport_data['rapport'] = rapport_list[12]
                            save_mysql(rapport_data)
                    except:
                        pass

                    try:
                        if rapport_list[0] == 'Trio':
                            rapport_data['rapport_type'] = 'trio'
                            rapport_data['horses'] = rapport_list[1]
                            rapport_data['rapport'] = rapport_list[2]
                            save_mysql(rapport_data)

                        if rapport_list[0] == 'Triordre':
                            rapport_data['rapport_type'] = 'triordre'
                            rapport_data['horses'] = rapport_list[1]
                            rapport_data['rapport'] = rapport_list[2]
                            save_mysql(rapport_data)
                    except:
                        pass
                    
                        

    # return rapport_simple

def save_mysql(data):
    """
    Save data to mysql
    """
    print("data to save: ", data)
    mysql = MySQL.MysqlUserDb()
    mysql.insert_data(data)
    mysql.close_connection()

    return

main(sys.argv)


