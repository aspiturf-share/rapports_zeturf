import mysql.connector
from mysql.connector import Error
import settings as settings

class MysqlUserDb:

    def __init__(self):
        try:
            self.connection = mysql.connector.connect(**settings.DB_CONNECT)
            if self.connection.is_connected():
                db_Info = self.connection.get_server_info()
                print("Connected to MySQL Server version ", db_Info)
                self.cursor = self.connection.cursor()

        except Error as e:
            print("Error while connecting to MySQL", e)

    def insert_data(self, data):
        try:
            self.cursor.execute("""INSERT INTO `rapportsze` (`rapport_type`, `horses`, `rapport`, `jour`, `reunion`, `course`) VALUES (%(rapport_type)s, %(horses)s, %(rapport)s, %(jour)s, %(reunion)s, %(course)s)""", data)
        except:
            self.cursor.execute("""UPDATE `rapportsze` SET `rapport_type` = %(rapport_type)s, `horses` = %(horses)s, `rapport` = %(rapport)s, `jour` = %(jour)s, `reunion` = %(reunion)s, `course` = %(course)s WHERE `jour` = %(jour)s AND `rapport_type` = %(rapport_type)s AND `reunion` = %(reunion)s AND `course` = %(course)s  """, data)
        

        self.connection.commit()

    def get_caractrap_id(self, jour, reun, prix):
        self.cursor.execute("""SELECT id  FROM `caractrap` WHERE `jour` = %s AND `reun` = %s AND `prix` = %s LIMIT 1""", (jour, reun, prix))

        return self.cursor.fetchone()

        self.connection.commit()


    def close_connection(self):
        self.cursor.close()
        self.connection.close()
        print("MySQL self.connection is closed")


