-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 04 jan. 2023 à 22:19
-- Version du serveur : 5.7.40
-- Version de PHP : 8.1.2-1ubuntu2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données : `pturf2`
--

-- --------------------------------------------------------

--
-- Structure de la table `rapportsze`
--

CREATE TABLE `rapportsze` (
  `jour` date NOT NULL,
  `reunion` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `rapport_type` varchar(45) NOT NULL,
  `horses` varchar(45) NOT NULL,
  `rapport` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `rapportsze`
--
ALTER TABLE `rapportsze`
  ADD UNIQUE KEY `jour` (`jour`,`reunion`,`course`,`rapport_type`);
COMMIT;
