# Rapports Zeturf

Récupère les rapports du jour du site zeturf.

- Renommer le fichier `settings-example.py` en `settings.py` et le compléter.

- La table doit se nomer `rapportsze`

- La structure de la table sql se trouve dans le fichier `rapportsze.sql`

Exécution du script:

`python3.10 main.py `